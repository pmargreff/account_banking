# Decisões

## Autorização

Essa parte foi a única que não tive total compreensão durante o teste. Ao mesmo tempo que ele pede pra identificar qual o cliente fez a requisição, não fala se deve ou não exigir autenticação. Assumi que as requisições precisam de autenticação para as ações (o que pode estar incorreto). As ações só podem ser feitas pelo dono do recurso, exemplo: o saldo de uma conta só pode ser visto por com o token gerado pela própria conta e a transferência só pode ser executada com o token da conta de origem da transação.

## Transações

As transações foram feitas usando `GenServer`, apesar de não ser o que seria escolhido para uma situação como essa (sem margem de falhas ou onde o histórico é crítico) é uma maneira razoável de fazer uma transação assíncrona sem o overhead de usar outras depedências.

Também decidi por usar apenas com inteiros para o saldo. Isso evita o possível gazilhão de problemas em se trabalhar com `float` ou com valores monetários reais.

## Possíveis problemas

O projeto possuí problemas clássicos de integridade em transações distribuídas. Caso seja executadas duas transações para a mesma conta existe a possibilidade que a conta tenha um estado incorreto por causa da maneira que as transações acontecem. Isso poderia ser corrigido de diferentes maneiras, a maneira mais simples usando tabela ETS que a própria BEAN oferece, esse *lock* também poderia ser feito a nível de banco de dados seja ele relacional ou semelhantes. O problema não foi resolvido no teste por uma questão de praticidade, uma vez que esse problema não é essencial de ser contornado em um teste teórico, e também poderia adicionar  complexidade desnecessária ao projeto.