[![pipeline status](https://gitlab.com/pmargreff/account_banking/badges/master/pipeline.svg)](https://gitlab.com/pmargreff/account_banking/commits/master)

## Dependencias

Para rodar o projeto você precisa ter instalado o [docker-compose](https://docs.docker.com/compose/install/).

## Instalação

Primeiramente crie o ecosistema (Elixir + Postgres + Aplicação) usando o docker-compose com o seguinte comando:

* `docker-compose up`

A primeira execução pode demorar alguns minutos pois seu computador pode precisar baixar as imagens da linguagem e do banco de dados e compilar todo projeto. Também na primeira inicialização são inseridas algumas entidades para facilitar o uso local (podem ser encontradas em `priv/repo/seeds.exs`). Tudo deve estar pronto pra rodar quando você ver a mensagem:

`backend_1  | app available`

## Execução

Após isso você poderá rodar os testes automatizados ou o servidor, rode os comandos (a aplicação que foi criada na etapa de instalação deve estar executando para execução dos comandos abaixo):

* Para rodar o servidor use `docker-compose exec backend mix phx.server`
* Para rodar os testes use `docker-compose exec backend mix test`

O serivdor deve estar disponível em [`localhost:4000`](http://localhost:4000).

## Migrações

* Para criar uma nova migração use `docker-compose exec backend mix ecto.gen.migration MigrationNames`

## Endpoints

### Autenticação

* POST `api/vi/authentications`

### Parâmetros

* `account_id`: Identificador da conta.

Todos os enpoints estão protegidos por autenticação. Apesar de não estar na especificação (ver mais no arquivo `/DECISOES.md`).

Para gerar um token de autorização envie uma requisição com o id da conta para o endpoint authorization (em um mundo real haveria algum sistema de checagem de autenticidade como senha). Os ids podem ser encontrados no arquivo `priv/repo/seeds.exs`, por praticidade eles são estáticos.


Exemplo:

```
curl --request POST \
  --url http://localhost:4000/api/v1/authentications \
  --header 'content-type: application/json' \
  --data '{ "account_id": "8bb545a8-1914-4530-a3c5-354123a06047" }'
```

A resposta será algo como:

```
{ "token": "mah_token"}
```

O token retornado deve ser usado nas próximas requisições no header da seguinte forma:

`authorization` - `Basic mah_token`

Somente tokens gerados pelas contas que possuem os recursos serão aceitos.

## Saldo

* GET `api/v1/accounts/balance`

### Parâmetros

* `account_id`: Identificador da conta.

```
curl --request GET \
  --url 'http://localhost:4000/api/v1/accounts/balance?account_id=account_id' \
  --header 'authorization: Token mah_token'
```

## Transações

* POST `api/vi/transactions/transfer`

### Parâmetros

* `source_account_id`: Conta de origem da transferência (string).
* `destination_account_id`: Conta de destino da transferência (string).
* `amount`: Valor da transferência em reais (inteiro).

Exemplo:

```
curl --request POST \
  --url http://localhost:4000/api/v1/transactions/transfer \
  --header 'authorization: Basic mah_token' \
  --header 'content-type: application/json' \
  --data '{
	"source_account_id": "account_1",
	"destination_account_id": "account_2",
	"amount": 10
}'
```