defmodule AccountBankingWeb.Authorizations do
  @secret Application.get_env(:account_banking, :authentication_secret)
  @salt Application.get_env(:account_banking, :authentication_salt)

  def create_token(resource_identifier),
    do: Phoenix.Token.sign(@secret, @salt, resource_identifier)
end
