defmodule AccountBankingWeb.TransactionsController do
  use AccountBankingWeb, :controller

  alias AccountBanking.Accounts.{
    Transactions,
    Transactions.Server
  }

  # TODO: dig into phoenix and check if it's a bug ¯\_(ツ)_/¯
  def transfer(conn, %{"amount" => amount} = params) when is_bitstring(amount) do
    transfer(conn, %{params | "amount" => String.to_integer(amount)})
  end

  def transfer(conn, %{
        "source_account_id" => source_account_id,
        "destination_account_id" => destination_account_id,
        "amount" => amount
      }) do
    with :ok <-
           Bodyguard.permit(Transactions, :transfer, conn.assigns[:account], source_account_id),
         :ok <-
           Server.transfer(source_account_id, destination_account_id, amount) do
      conn
      |> put_status(202)
      |> json(%{"success" => "your transfer will be processed"})
    else
      {:error, :unauthorized} -> unauthorized(conn)
      {:error, :forbidden} -> forbidden(conn)
    end
  end

  defp forbidden(conn) do
    conn
    |> put_status(:forbidden)
    |> put_view(AccountBankingWeb.ErrorView)
    |> render(:"403")
  end

  defp unauthorized(conn) do
    conn
    |> put_status(:unauthorized)
    |> put_view(AccountBankingWeb.ErrorView)
    |> render(:"401")
  end
end
