defmodule AccountBankingWeb.AccountsController do
  use AccountBankingWeb, :controller

  alias AccountBanking.Accounts

  def balance(conn, %{"account_id" => account_id}) do
    with :ok <- Bodyguard.permit(Accounts, :view_balance, conn.assigns[:account], account_id),
         {:ok, account} <- Accounts.get(account_id) do
      balance = Accounts.balance(account)

      conn
      |> json(%{"balance" => balance})
    else
      {:error, :unauthorized} -> unauthorized(conn)
      {:error, :forbidden} -> forbidden(conn)
    end
  end

  defp forbidden(conn) do
    conn
    |> put_status(:forbidden)
    |> put_view(AccountBankingWeb.ErrorView)
    |> render(:"403")
  end

  defp unauthorized(conn) do
    conn
    |> put_status(:unauthorized)
    |> put_view(AccountBankingWeb.ErrorView)
    |> render(:"401")
  end
end
