defmodule AccountBankingWeb.AuthenticationController do
  use AccountBankingWeb, :controller

  alias AccountBanking.Accounts
  alias AccountBankingWeb.Authorizations

  def generate_token(conn, %{"account_id" => account_id}) do
    # NOTE: here we should verify the password when if I've one
    with {:ok, _account} <- Accounts.get(account_id) do
      token = Authorizations.create_token(account_id)

      conn
      |> json(%{token: token})
    else
      {:error, :not_found} ->
        conn
        |> put_status(:not_found)
        |> put_view(AccountBankingWeb.ErrorView)
        |> render(:"404")
    end
  end
end
