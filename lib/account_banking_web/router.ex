defmodule AccountBankingWeb.Router do
  use AccountBankingWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug(AccountBankingWeb.AuthorizationPlug)
  end

  scope "/api", AccountBankingWeb do
    pipe_through :api

    scope "/v1" do
      post("/transactions/transfer", TransactionsController, :transfer)
      get("/accounts/balance", AccountsController, :balance)
      post("/authentications", AuthenticationController, :generate_token)
    end
  end
end
