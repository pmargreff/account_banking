defmodule AccountBankingWeb.AuthorizationPlug do
  @moduledoc """
  Module to implement api authorization behaviour
  """
  import Plug.Conn

  alias AccountBanking.Accounts

  @secret Application.get_env(:account_banking, :authentication_secret)
  @salt Application.get_env(:account_banking, :authentication_salt)

  def init(args), do: args

  def call(conn, _args) do
    with {:ok, account} <- authorize(conn) do
      assign(conn, :account, account)
    else
      _ ->
        assign(conn, :account, nil)
    end
  end

  @day_in_miliseconds 186_000

  defp authorize(conn) do
    with ["Basic " <> token] <- get_req_header(conn, "authorization"),
         {:ok, account_id} <-
           Phoenix.Token.verify(@secret, @salt, token, max_age: @day_in_miliseconds) do
      Accounts.get(account_id)
    end
  end
end
