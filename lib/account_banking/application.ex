defmodule AccountBanking.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    children =
      [
        # Start the Ecto repository
        AccountBanking.Repo,
        # Start the endpoint when the application starts
        AccountBankingWeb.Endpoint
      ] ++ extra_applications(Mix.env())

    opts = [strategy: :one_for_one, name: AccountBanking.Supervisor]

    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    AccountBankingWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp extra_applications(:test), do: []

  defp extra_applications(_),
    do: [{AccountBanking.Accounts.Transactions.Server, []}]
end
