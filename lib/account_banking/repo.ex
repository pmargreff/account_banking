defmodule AccountBanking.Repo do
  use Ecto.Repo,
    otp_app: :account_banking,
    adapter: Ecto.Adapters.Postgres
end
