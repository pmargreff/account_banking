defmodule AccountBanking.Accounts.Account do
  @moduledoc """
  Model for account.
  """
  use Ecto.Schema

  import Ecto.Changeset

  alias AccountBanking.Accounts.Transaction

  @primary_key {:uuid, :binary_id, autogenerate: true}

  @foreign_key_type :binary_id

  schema "accounts" do
    field :number, :string

    has_many :transactions, Transaction,
      foreign_key: :account_uuid,
      references: :uuid

    timestamps()
  end

  @required ~w(number)a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required)
    |> validate_required(@required)
  end
end
