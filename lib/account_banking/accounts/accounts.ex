defmodule AccountBanking.Accounts do
  alias AccountBanking.{
    Accounts.Account,
    Accounts.Transactions,
    Repo
  }

  defdelegate authorize(action, user, params), to: __MODULE__.Policy

  def get(uuid) do
    Repo.get(Account, uuid)
    |> case do
      nil -> {:error, :not_found}
      account -> {:ok, account}
    end
  end

  def balance(account) do
    Transactions.by_account(account)
    |> Enum.reduce(0, fn transaction, acc ->
      acc + Map.get(transaction, :value, 0)
    end)
  end
end
