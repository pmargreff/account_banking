defmodule AccountBanking.Accounts.Transactions.Queries do
  alias AccountBanking.Accounts.Transaction

  import Ecto.Query, only: [from: 2]

  def by_account(query \\ Transaction, account_uuid),
    do: from(query, where: [account_uuid: ^account_uuid])
end
