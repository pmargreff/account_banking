defmodule AccountBanking.Accounts.Transactions.Server do
  @moduledoc """
  GenServer for handle account transactions.
  """

  use GenServer

  alias AccountBanking.Accounts.Transactions

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    {:ok, nil}
  end

  def transfer(source_account, destination_account, value) do
    GenServer.cast(__MODULE__, {:transfer, source_account, destination_account, value})
  end

  def handle_cast({:transfer, source_account, destination_account, value}, state) do
    Transactions.transfer(source_account, destination_account, value)
    {:noreply, state}
  end
end
