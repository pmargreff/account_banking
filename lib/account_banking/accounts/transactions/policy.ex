defmodule AccountBanking.Accounts.Transactions.Policy do
  @moduledoc """
  Policy module for transactions permissions
  """

  alias AccountBanking.Accounts.Account

  def authorize(:transfer, %Account{uuid: id}, id), do: :ok
  def authorize(_, nil, _), do: {:error, :unauthorized}
  def authorize(_, _, _), do: {:error, :forbidden}
end
