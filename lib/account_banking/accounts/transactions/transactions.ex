defmodule AccountBanking.Accounts.Transactions do
  alias AccountBanking.{
    Accounts,
    Accounts.Transaction,
    Accounts.Transactions.Queries,
    Repo
  }

  defdelegate authorize(action, user, params), to: __MODULE__.Policy

  def transfer(_source_account, _destination_account, value) when value < 0 do
    {:error, :invalid_value}
  end

  def transfer(source_account, destination_account, value) do
    correlation_id = Ecto.UUID.generate()

    Ecto.Multi.new()
    |> Ecto.Multi.run(:validate_source_account_balance, fn _repo, _params ->
      has_balance_to_transfer?(source_account, value)
    end)
    |> Ecto.Multi.run(:insert_source_transaction, fn _repo, _params ->
      source_account
      |> create_transaction_params(-value, correlation_id)
      |> insert_transfer()
    end)
    |> Ecto.Multi.run(:insert_destination_transaction, fn _repo, _params ->
      destination_account
      |> create_transaction_params(value, correlation_id)
      |> insert_transfer()
    end)
    |> Repo.transaction()
  end

  def insert_transfer(params) do
    %Transaction{}
    |> Transaction.transfer_changeset(params)
    |> Repo.insert()
  end

  def by_account(%{uuid: account_uuid}) do
    Queries.by_account(account_uuid)
    |> Repo.all()
  end

  defp create_transaction_params(account_uuid, value, correlation_id) do
    %{
      account_uuid: account_uuid,
      value: value,
      correlation_id: correlation_id
    }
  end

  defp has_balance_to_transfer?(account_uuid, ammount_to_transfer) do
    Accounts.get(account_uuid)
    |> case do
      {:ok, account} ->
        if Accounts.balance(account) >= ammount_to_transfer do
          {:ok, true}
        else
          {:error, :insuficient_balance}
        end

      {:error, _error} ->
        {:error, :insuficient_balance}
    end
  end
end
