defmodule AccountBanking.Accounts.Transaction do
  @moduledoc """
  Model for transaction.
  """
  use Ecto.Schema

  import Ecto.Changeset

  alias AccountBanking.Accounts.Account

  @primary_key {:uuid, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "transactions" do
    field :value, :integer
    field :correlation_id, Ecto.UUID

    belongs_to :account, Account,
      references: :uuid,
      foreign_key: :account_uuid,
      type: Ecto.UUID

    timestamps()
  end

  @transfer_required ~w(value account_uuid correlation_id)a

  def transfer_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @transfer_required)
    |> validate_required(@transfer_required)
    |> assoc_constraint(:account)
  end
end
