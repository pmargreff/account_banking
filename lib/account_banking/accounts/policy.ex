defmodule AccountBanking.Accounts.Policy do
  @moduledoc """
  Policy module for accounts permissions
  """

  alias AccountBanking.Accounts.Account

  def authorize(:view_balance, %Account{uuid: id}, id), do: :ok
  def authorize(_, nil, _), do: {:error, :unauthorized}
  def authorize(_, _, _), do: {:error, :forbidden}
end
