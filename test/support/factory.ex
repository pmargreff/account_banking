defmodule AccountBanking.Factory do
  use ExMachina.Ecto, repo: AccountBanking.Repo

  def account_factory do
    %AccountBanking.Accounts.Account{
      number: sequence(:number, &"00#{&1}-0")
    }
  end

  def with_balance(account, value) do
    insert(:transaction, account: account, value: value)
    account
  end

  def transaction_factory do
    %AccountBanking.Accounts.Transaction{
      value: Enum.random(1..10) * 10,
      correlation_id: Ecto.UUID.generate(),
      account: build(:account)
    }
  end
end
