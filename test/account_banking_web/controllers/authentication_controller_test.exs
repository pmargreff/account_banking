defmodule AccountBankingWeb.AuthenticationControllerTest do
  use AccountBankingWeb.ConnCase

  import AccountBanking.Factory

  setup %{conn: conn} do
    conn = put_req_header(conn, "accept", "application/json")

    {:ok, conn: conn}
  end

  describe "authorization" do
    test "get authorization when account exists", %{conn: conn} do
      %{uuid: account_id} = insert(:account)

      conn = post(conn, Routes.authentication_path(conn, :generate_token, account_id: account_id))

      assert %{"token" => _token} = json_response(conn, 200)
    end

    test "fails when account does not exists", %{conn: conn} do
      conn =
        post(
          conn,
          Routes.authentication_path(conn, :generate_token, account_id: Ecto.UUID.generate())
        )

      assert json_response(conn, 404) == %{"errors" => %{"detail" => "Not Found"}}
    end
  end
end
