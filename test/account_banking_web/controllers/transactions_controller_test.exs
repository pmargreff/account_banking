defmodule AccountBankingWeb.TransactionsControllerTest do
  use AccountBankingWeb.ConnCase

  import AccountBanking.Factory

  setup %{conn: conn} do
    account =
      insert(:account)
      |> with_balance(1000)

    conn = put_req_header(conn, "accept", "application/json")
    account_conn = login_as(conn, account)
    {:ok, account: account, account_conn: account_conn, conn: conn}
  end

  describe "transfer" do
    test "succeeds on transfer", %{account: source_account, account_conn: conn} do
      destination_account = insert(:account)

      payload = %{
        "source_account_id" => source_account.uuid,
        "destination_account_id" => destination_account.uuid,
        "amount" => 100
      }

      conn = post(conn, Routes.transactions_path(conn, :transfer, payload))

      assert json_response(conn, 202) == %{"success" => "your transfer will be processed"}
    end

    test "fails when has no authentication header", %{account: source_account, conn: conn} do
      destination_account = insert(:account)

      payload = %{
        "source_account_id" => source_account.uuid,
        "destination_account_id" => destination_account.uuid,
        "amount" => 100
      }

      conn = post(conn, Routes.transactions_path(conn, :transfer, payload))

      assert json_response(conn, 401) == %{"errors" => %{"detail" => "Unauthorized"}}
    end
  end
end
