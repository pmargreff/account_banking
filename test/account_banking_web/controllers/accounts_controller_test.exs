defmodule AccountBankingWeb.AccountControllerTest do
  use AccountBankingWeb.ConnCase

  import AccountBanking.Factory

  setup %{conn: conn} do
    account = insert(:account)

    conn = put_req_header(conn, "accept", "application/json")
    account_conn = login_as(conn, account)
    {:ok, account: account, account_conn: account_conn, conn: conn}
  end

  describe "balance" do
    test "succeds to get balance", %{account: account, account_conn: conn} do
      insert(:transaction, account: account, value: 1000)

      conn = get(conn, Routes.accounts_path(conn, :balance, account_id: account.uuid))

      assert json_response(conn, 200) == %{"balance" => 1000}
    end

    test "fails when has no authentication header", %{conn: conn} do
      conn = get(conn, Routes.accounts_path(conn, :balance, account_id: Ecto.UUID.generate()))

      assert json_response(conn, 401) == %{"errors" => %{"detail" => "Unauthorized"}}
    end
  end
end
