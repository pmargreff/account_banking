defmodule AccountBankingWeb.AuthorizationPlugTest do
  use AccountBankingWeb.ConnCase

  import AccountBanking.Factory

  alias AccountBankingWeb.{
    Authorizations,
    AuthorizationPlug
  }

  describe "call" do
    test "with valid account authorization sets account on conn" do
      account = insert(:account)

      token = Authorizations.create_token(account.uuid)

      conn =
        build_conn()
        |> put_req_header("authorization", "Basic #{token}")
        |> AuthorizationPlug.call(%{})

      assert conn.assigns[:account] == account
    end

    test "with no authorizations sets conn's account as nil" do
      conn =
        build_conn()
        |> AuthorizationPlug.call(%{})

      assert conn.assigns[:account] == nil
    end

    test "with invalid authorization sets account as nil" do
      conn =
        build_conn()
        |> put_req_header("authorization", "Basic invalid_token")
        |> AuthorizationPlug.call(%{})

      assert conn.assigns[:account] == nil
    end
  end
end
