defmodule AccountBanking.Accounts.Transactions.ServerTest do
  use AccountBanking.DataCase

  alias AccountBanking.Accounts.Transactions.Server, as: TransactionsServer

  describe "transfer/3" do
    test "enqueue a transfer to run" do
      source_account =
        insert(:account)
        |> with_balance(1000)

      destination_account = insert(:account)

      {:ok, _server_pid} = TransactionsServer.start_link(TransactionsServer)
      assert :ok == TransactionsServer.transfer(source_account, destination_account, 100)
    end
  end
end
