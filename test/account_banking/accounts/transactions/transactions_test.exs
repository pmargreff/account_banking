defmodule AccountBanking.Accounts.TransactionsTest do
  use AccountBanking.DataCase

  alias AccountBanking.{
    Accounts.Transaction,
    Accounts.Transactions,
    Repo
  }

  import Ecto.Query

  describe "insert_transfer/2" do
    test "insert a transfer with a valid account" do
      %{uuid: account_uuid} = insert(:account)

      params = params_for(:transaction, account_uuid: account_uuid)

      assert {:ok, new_transaction} = Transactions.insert_transfer(params)
      assert [new_transaction] = Repo.all(Transaction)
    end

    test "does not insert transfer with an invalid account" do
      params = %{
        account_uuid: "001-0",
        value: 1000,
        correlation_id: Ecto.UUID.generate()
      }

      assert {:error, new_transaction} = Transactions.insert_transfer(params)
      assert [] = Repo.all(Transaction)
    end
  end

  describe "transfer/3" do
    test "for a valid transfer, source receives a negative transaction" do
      source_account =
        insert(:account)
        |> with_balance(1000)

      destination_account = insert(:account)

      assert {:ok, multi_transactions} =
               Transactions.transfer(source_account.uuid, destination_account.uuid, 100)

      assert transaction =
               from(Transaction, where: [account_uuid: ^source_account.uuid])
               |> first(:value)
               |> Repo.one()

      assert transaction.value == -100
    end

    test "for a valid transfer, destination receives a positive transaction" do
      source_account =
        insert(:account)
        |> with_balance(1000)

      destination_account = insert(:account)

      assert {:ok, multi_transactions} =
               Transactions.transfer(source_account.uuid, destination_account.uuid, 100)

      assert transaction = Repo.get_by(Transaction, account_uuid: destination_account.uuid)
      assert transaction.value == 100
    end

    test "cannot transfer a value higher than current balance" do
      source_account =
        insert(:account)
        |> with_balance(1000)

      destination_account = insert(:account)

      assert {:error, :validate_source_account_balance, :insuficient_balance, %{}} =
               Transactions.transfer(source_account.uuid, destination_account.uuid, 2000)
    end

    test "cannot transfer a negative value" do
      source_account =
        insert(:account)
        |> with_balance(1000)

      destination_account = insert(:account)

      assert {:error, :invalid_value} =
               Transactions.transfer(source_account.uuid, destination_account.uuid, -100)
    end
  end
end
