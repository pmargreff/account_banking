defmodule AccountBanking.AccountsTest do
  use AccountBanking.DataCase

  alias AccountBanking.Accounts

  describe "balance/1" do
    test "returns zero as balance when account has no transactions" do
      account = insert(:account)

      assert Accounts.balance(account) == 0
    end

    test "calculate correct balance for multiple transactions" do
      account =
        insert(:account)
        |> with_balance(1000)

      insert(:transaction, account: account, value: -250)

      assert Accounts.balance(account) == 750
    end
  end
end
