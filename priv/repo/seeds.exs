alias AccountBanking.{
  Accounts.Account,
  Accounts.Transaction,
  Repo
}

import AccountBanking.Factory

Repo.delete_all(Account)
Repo.delete_all(Transaction)

account_1 = insert(:account, uuid: "645261f4-cea9-4b77-9afd-b070041aedad") |> with_balance(1000)
account_2 = insert(:account, uuid: "61ef05dd-991e-4dc0-8a34-6baf21f33a18") |> with_balance(1000)
account_3 = insert(:account, uuid: "8bb545a8-1914-4530-a3c5-354123a06047") |> with_balance(1000)

transaction_correlation_id_1 = Ecto.UUID.generate()
transaction_correlation_id_2 = Ecto.UUID.generate()

_transaction_1 =
  insert(:transaction,
    value: 500,
    account: account_1,
    correlation_id: transaction_correlation_id_1
  )

_transaction_2 =
  insert(:transaction,
    value: -500,
    account: account_2,
    correlation_id: transaction_correlation_id_1
  )

_transaction_3 =
  insert(:transaction,
    value: 1000,
    account: account_2,
    correlation_id: transaction_correlation_id_2
  )

_transaction_4 =
  insert(:transaction,
    value: -1000,
    account: account_3,
    correlation_id: transaction_correlation_id_2
  )

"""
  after transactions the balances are:
  account 1: 1500,
  account 2: 1500,
  account 3: 0
"""
