defmodule AccountBanking.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions, primary_key: false) do
      add :uuid, :uuid, primary_key: true
      add :value, :integer, null: false
      add :correlation_id, :uuid
      add :account_uuid, references(:accounts, type: :uuid, column: :uuid)

      timestamps()
    end
  end
end
