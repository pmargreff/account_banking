# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :account_banking,
  ecto_repos: [AccountBanking.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :account_banking, AccountBankingWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "BYYlUu6TSBea7dWDXzv5dQJkBHOfeAjK1yC6bhy578s0JfSA5g62KscSxaP04EPh",
  render_errors: [view: AccountBankingWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: AccountBanking.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
