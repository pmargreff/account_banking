use Mix.Config

# Configure your database
config :account_banking, AccountBanking.Repo,
  username: System.get_env("POSTGRES_USERNAME") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  port: String.to_integer(System.get_env("POSTGRES_PORT") || "5432"),
  hostname: System.get_env("POSTGRES_HOSTNAME") || "localhost",
  database: "account_banking_test",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :account_banking, AccountBankingWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :account_banking,
  authentication_secret: "kjoy3o1zeidquwy1398juxzldjlksahdk3",
  authentication_salt: "test"
